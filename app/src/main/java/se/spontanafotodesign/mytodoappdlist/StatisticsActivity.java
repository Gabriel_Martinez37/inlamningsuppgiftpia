package se.spontanafotodesign.mytodoappdlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

public class StatisticsActivity extends AppCompatActivity {



    public static final String KEY_TASK = "TASKS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


     ArrayList<Task> tasks =   getIntent().getParcelableArrayListExtra(TaskListActivity.KEY_TASKS);


        Date youngest = new Date(0);
        Date oldest = new Date(Long.MAX_VALUE);



        int total = tasks.size();
        int completed = 0;
        int archive = 0;
        for (int i = 0; i < tasks.size(); i++) {
            Task task = tasks.get(i);

           Date started = task.getStarted();
                if (started.after(youngest)){

                    youngest = started;

                }


                if (started.before(oldest)){

                    oldest = started;

                }


                if (task.isCompleted()){
                    completed++;

                }

                if (task.isArchived()){
                    archive++;
                }
        }


        TextView tasksTotal = (TextView) findViewById(R.id.tasks_total);
        tasksTotal.setText(String.valueOf(total));

        TextView tasksCompleted = (TextView) findViewById(R.id.tasks_completed);
        tasksCompleted.setText(String.valueOf(completed));

        TextView tasksArchived = (TextView) findViewById(R.id.tasks_archived);
        tasksArchived.setText(String.valueOf(archive));

        double timeDiff = youngest.getTime() - oldest.getTime();
        double days = (timeDiff / (1000 * 60 * 60 * 24)) + 1;


        TextView avgPerDay = (TextView) findViewById(R.id.average_per_day);
        avgPerDay.setText(String.valueOf(total / days));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
