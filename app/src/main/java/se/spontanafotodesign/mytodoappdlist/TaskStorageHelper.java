package se.spontanafotodesign.mytodoappdlist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteAbortException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public final class TaskStorageHelper implements Handler.Callback {
    private static final TaskStorageHelper INSTANCE = new TaskStorageHelper();
    private static final String TAG = "TaskStorageHelper";
    private static final int MSG_SAVE_TASK = 10;
    private static final int MSG_LOAD_TASKS = 20;
    private final Handler bgHandler;
    private DbHelper dbHelper;
    private TaskStorageHelper() {
        HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
         bgHandler = new Handler(handlerThread.getLooper(), this);



    }
    public void initStorage(Context context) {

        dbHelper = new DbHelper(context);

    }

    public static TaskStorageHelper getInstance() {return INSTANCE;}

    public void saveTask(Task task) {

      Message message =  bgHandler.obtainMessage(MSG_SAVE_TASK, task);
        message.sendToTarget();

    }


    public void getTasks(Callback callback) {
        Message message = bgHandler.obtainMessage(MSG_LOAD_TASKS, callback);
        message.sendToTarget();
    }


    public interface Callback{

        void onData(List<Task> tasks);

    }

    @Override
    public boolean handleMessage(Message msg) {
        if (msg.what == MSG_SAVE_TASK){
            internalSaveTask((Task)msg.obj);
        } else if(msg.what == MSG_LOAD_TASKS){
            
            internalLoadTasks((Callback)msg.obj);
            
        }
        return true;
    }

    private void internalLoadTasks(Callback callback) {

      SQLiteDatabase database =  dbHelper.getReadableDatabase();
      Cursor cursor =  database.query("task", DBSchema.COLUMNS, null, null, null , null, null);






    }

    private void internalSaveTask(Task task) {
       SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put("title", task.getTitle());
        values.put("description", task.getDescription());
        values.put("started", task.getStarted().getTime());
        values.put("completed", task.isCompleted());
        values.put("archived", task.isArchived());

        if(task.getId() == 0){

           database.insert("task", "", values);
       }else{
            database.update("task",values , "_id = ?", new String[]{String.valueOf(task.getId())});
       }
    }


    public static class DBSchema {

        public static final String CREATE_TASK_TABLE =
                "CREATE TABLE task (" +

                        "_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "title TEXT," +
                        "description TEXT," +
                        "started INTEGER," +
                        "completed INTEGER," +
                        "archived INTEGER" +

                        ")";

        public static final String[] COLUMNS = {"_id", "title", "description", "started", "completed", "archived"};

    }


    private class DbHelper extends SQLiteOpenHelper{
        private static final String DB_NAME = "tasks.db";
        private static final int DB_VERSION = 1;
        public DbHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DBSchema.CREATE_TASK_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
